FROM mcr.microsoft.com/dotnet/sdk:3.1 AS build-env
WORKDIR /app
RUN mkdir /Ms.Cross.Jwt
RUN mkdir /Ms.Cross.Security

# Copy csproj and restore as distinct layers
COPY Ms.Cross.Jwt/*.csproj  Ms.Cross.Jwt/
COPY Ms.Cross.Security/*.csproj  Ms.Cross.Security/
COPY Ms.Pediakids.Core/*.csproj ./
#COPY ../Ms.Cross.Jwt/*.csproj Ms.Cross.Jwt/

RUN dotnet restore

# Copy everything else and build
COPY ./ ./
RUN dotnet publish Ms.Pediakids.Core/ -c Release -o publish


# Build runtime image
FROM mcr.microsoft.com/dotnet/core/aspnet:3.1-buster-slim
WORKDIR /app

COPY --from=build-env  /app/publish .
ENV  ASPNETCORE_URLS=http://+:80
ENV  ASPNETCORE_ENVIRONMENT Production
EXPOSE 80

ENTRYPOINT ["dotnet", "/app/Pediakids.dll"]