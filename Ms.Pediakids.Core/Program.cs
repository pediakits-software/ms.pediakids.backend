using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Ms.Cross.Security.Data.Seeder;

namespace Ms.Pediakids.Core
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = BuildWebHost(args);
            RunSeeding(host);
            BuildWebHost(args).Run();
            //CreateHostBuilder(args).Build().Run();
        }

        /* public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                }); */
        private static void RunSeeding(IWebHost host)
        {
            var scopeFactory = host.Services.GetService<IServiceScopeFactory>();
            using (var scope = scopeFactory.CreateScope())
            {
                var seeder = scope.ServiceProvider.GetService<SecuritySeeder>();
                seeder.SeedAsync().Wait();
            }
        }

        public static IWebHost BuildWebHost(string[] args) =>

            /* WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>().Build(); */
            WebHost.CreateDefaultBuilder(args)
               //For https:
               //.UseKestrel(options => options.Listen(System.Net.IPAddress.Any, 5000, listenOptions =>
               //listenOptions.UseHttps("server.pfx", "radzen-https-password")))
               //.UseUrls("http://*:5000")
               .UseStartup<Startup>()
               .Build();
    }
}
