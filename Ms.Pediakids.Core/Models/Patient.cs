
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Ms.Cross.Security.Models;
using System.Collections.Generic;

namespace Ms.Pediakids.Core.Models
{
    [Table("Patient", Schema = "public")]
    public partial class Patient:AuditableEntity
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [StringLength(50)]
        public string Nombres { get; set; }    
        [StringLength(50)]
        [Required]
        public string Apellidos { get; set; }
        public string NombreCompleto { get; set; }  
        [Required]
        [StringLength(20)]
        public string NumeroIdentificacion { get; set; }
        [Required]
        public DateTime FechaNacimiento { get; set; }
        [StringLength(100)]
        public string Direccion { get; set; }
        [StringLength(20)]
        public string Telefono { get; set; }

            
        // SET EXTERNAL RELATIONS 
        //============================================================= 
        public int TipoIdentificacionId { get; set;} 
        [ForeignKey("TipoIdentificacionId")] 
        public ItemList TipoIdentificacion { get; set;} 

        public int GeneroId { get; set;} 
        [ForeignKey("GeneroId")] 
        public ItemList Genero { get; set;} 

         // ICollections 
        //============================================================= 
        [InverseProperty("Patient")] 
        public ICollection<MedicalConsultation> MedicalConsultations { get; set;}


    }
}