using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Ms.Cross.Security.Models;

namespace Ms.Pediakids.Core.Models
{
    [Table("MedicalConsultation", Schema = "public")]
    public partial class MedicalConsultation:AuditableEntity
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [StringLength(20)]
        public string NumeroIdentificacion { get; set; }
        [Required]  
        public string Motivo { get; set; }
        
        [Required]
        public string EvolucionEnfermedad { get; set; }

        [Required]
        public string Antecedentes { get; set; }

        //examen fisico 
        [Required]
        [StringLength(20)]
        public string Peso { get; set; }
        
        [Required]
        public int Talla { get; set; }
        
        [Required]
        public double PerimetoCefalico{ get; set; }

        //signos vitales 
        public int FrecuenciaCardiaca { get; set; }
        public int FrecuenciaResiratorio { get; set; }
        public double Temperatura { get; set; }

        public int? Saturacion { get; set; }

        [StringLength(7)]
        public string TencionArterial { get; set; }
        [StringLength(500)]
        public string Cabeza { get; set; }
        [StringLength(500)]
        public string ORL { get; set; }
        [StringLength(500)]
        public string Cuello { get; set; }
        
        [StringLength(500)]
        public string Torax { get; set; }
        [StringLength(500)]
        public string Abdomen { get; set; }
        [StringLength(500)]
        public string Genitales { get; set; }
        [StringLength(500)]
        public string Extremidades { get; set; }
        [StringLength(500)]
        public string Piel { get; set; }
        [StringLength(500)]
        public string Neurologico { get; set; }

        [Required]
        public string Diagnostico { get; set; }
        public string Tratamiento { get; set; }
        public string Paraclinicos { get; set; }
        public string ParaclinicosOpcional { get; set; }

        // SET EXTERNAL RELATIONS 
        public int PatientId { get; set;} 
        [ForeignKey("PatientId")] 
        public Patient Patient { get; set;} 

        public int TipoIdentificacionId { get; set;} 
        [ForeignKey("TipoIdentificacionId")] 
        public ItemList TipoIdentificacion { get; set;} 

    }
}