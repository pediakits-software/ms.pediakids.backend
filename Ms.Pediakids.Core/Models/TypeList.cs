using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Ms.Cross.Security.Models;

namespace Ms.Pediakids.Core.Models
{
    [Table("TypeList", Schema = "public")]
    public partial class TypeList:AuditableEntity
    {
        [Key] public int Id { get; set; }
        [StringLength(40)]
        public string Name { get; set; }
        public string Description { get; set; }

        // ICollections 
        //============================================================= 
        [InverseProperty("TypeList")] 
        public ICollection<ItemList> ItemsList { get; set;} 
    }
}