using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Ms.Cross.Security.Models;

namespace Ms.Pediakids.Core.Models
{
    [Table("ItemList", Schema = "public")]
    public partial class ItemList:AuditableEntity
    {
        [Key] public int Id { get; set; }
        public string Name { get; set; }
        [Column("Properties",TypeName = "json")]
        public string Properties { get; set; }
        [StringLength(30)]
        public string Code { get; set; }
        public bool? Active { get; set; }

        
        // SET EXTERNAL RELATIONS 
        //============================================================= 
        public int TypeListId { get; set;} 
        [ForeignKey("TypeListId")] 
        public TypeList TypeList { get; set;} 

        public int? Owner_Id { get; set;} 
        [ForeignKey("Owner_Id")] 
        public ItemList ItemListOwner { get; set;} 

        // ICollections 
        //============================================================= 
        [InverseProperty("ItemListOwner")] 
        public ICollection<ItemList> ItemListChilds { get; set;}

        [InverseProperty("TipoIdentificacion")] 
        public ICollection<Patient> Patients { get; set;}
        
        [InverseProperty("TipoIdentificacion")] 
        public ICollection<MedicalConsultation> MedicalConsultations { get; set;}
        
        [InverseProperty("Genero")] 
        public ICollection<Patient> PatientGeneros { get; set;}

    }
}