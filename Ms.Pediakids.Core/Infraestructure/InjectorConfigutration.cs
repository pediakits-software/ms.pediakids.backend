using Microsoft.Extensions.DependencyInjection;
using Ms.Pediakids.Core.Services.Mapper;

namespace Ms.Pediakids.Core.Infraestructure
{
    public static class InjectorConfigutration
    {
        public static IServiceCollection ConfigureInjectors(this IServiceCollection services){

            
            //Mappers
            services.AddScoped<ApplicationUserMapper>();
            services.AddScoped<ApplicationPatientMapper>();

            return services;
        }
    }
}