using System;
using System.Linq;
using Microsoft.AspNet.OData.Builder;
using Microsoft.AspNet.OData.Extensions;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OData.Edm;
using Ms.Cross.Security.Models;
using Ms.Cross.Security.Src;
using Ms.Pediakids.Core.Models;

namespace Ms.Pediakids.Core.Infraestructure
{
    public static class OdataConfiguration
    {
        public static IEdmModel GetEdmModel(IServiceProvider provider)
        {
            var oDataBuilder = new ODataConventionModelBuilder(provider);
            oDataBuilder.EntitySet<ItemList>("ItemLists");
            oDataBuilder.EntitySet<TypeList>("TypeLists");
            oDataBuilder.EntitySet<Patient>("Patients");
            oDataBuilder.EntitySet<MedicalConsultation>("MedicalConsultations");

            SecurityOdataConfiguration.SetModelIdentityOdataBuilder(provider, oDataBuilder);
            return oDataBuilder.GetEdmModel();
        }
    }
}