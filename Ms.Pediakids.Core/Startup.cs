using System;
using System.Text.Json.Serialization;
using Microsoft.AspNet.OData.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Ms.Cross.Jwt.Src;
using Ms.Cross.Security.Data;
using Ms.Cross.Security.Src;
using Ms.Pediakids.Core.Data.Migrations;
using Ms.Pediakids.Core.Infraestructure;
using Ms.Pediakids.Core.Services.Socket;
using Newtonsoft.Json;

namespace Ms.Pediakids.Core
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services
            .AddControllers(options => options.EnableEndpointRouting = false)
            .AddJsonOptions(opts =>
            {
                opts.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
            });
            services.AddOptions();
            services.AddMvc().AddNewtonsoftJson(o => 
            {
                o.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            });
            services.AddHttpContextAccessor();
            /*Start - JWT*/
            services.Configure<JwtOptions>(Configuration.GetSection("jwt"));
            services.AddJwtCustomized();
            /*End- JWT*/

            /*Start Security*/
            services.AddSecurityCorsConfig(); // Cors
            services.AddSecurityOdataConfig();  // Odata Rest
            services.AddSecurityIdentityConfig();  // Identity
            services.AddSecuritySwaguer();  // Swaguer
            /*end Identity*/

            /*Start DB Context Security*/
            services.AddDbContext<SecurityDbContext>(options => {
               options.UseNpgsql(
                   Configuration.GetConnectionString("bdsource"),
                   b => b.MigrationsAssembly("Pediakids"));
                options.EnableSensitiveDataLogging();
                   
           }, ServiceLifetime.Transient);
            /*End Context Security*/
            /*Start DB Context App*/
            services.AddDbContext<AppDohDbContext>(options =>
            {
                options.UseNpgsql(Configuration.GetConnectionString("bdsource"),
                   b => b.MigrationsAssembly("Pediakids"));
            }, ServiceLifetime.Transient);
            /*End DB Context App*/
            /*Start Services*/
            services.ConfigureInjectors();
            /*end Services*/

            //SET SIGNALR
            //services.AddSignalR(); // for websocket

            //CONFIG CACHE
            // cache in memory
            services.AddMemoryCache();
            // caching response for middlewares
            services.AddResponseCaching();

            // Multipart body length limit exceeded exception
            services.Configure<FormOptions>(x => {
                x.ValueLengthLimit = int.MaxValue;
                x.MultipartBodyLengthLimit = int.MaxValue; // In case of multipart
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            IServiceProvider provider = app.ApplicationServices.GetRequiredService<IServiceProvider>();
            /*Start - Cors*/
            app.UseCors(SecurityCorsConfiguration.MyPolicy);
            /*End - Cors*/
            app.UseDefaultFiles();
            app.UseStaticFiles();
            //app.UseSession();
            app.UseForwardedHeaders();
            //app.UseHttpsRedirection();
            //CACHE
            app.UseResponseCaching();
            app.UseRouting();
            app.UseAuthorization();

            // End Swaguer Documentacion
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Foo API V1");
                //c.RoutePrefix = string.Empty;
                c.DefaultModelRendering(Swashbuckle.AspNetCore.SwaggerUI.ModelRendering.Model);
            });

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                //endpoints.MapHub<SignalrDemoHub>("/signalrhub");  // the SignalrDemoHub url
            });

            app.UseMvc(routeBuilder =>
            {
                /* if (env.EnvironmentName == "Development")
                {
                    
                } */
                routeBuilder.MapRoute(name: "default",
                    template: "{controller}/{action}/{id?}",
                    defaults: new { controller = "Home", action = "Index" }
                );

                routeBuilder.Count().Filter().OrderBy().Expand().Select().MaxTop(null).SetTimeZoneInfo(TimeZoneInfo.Utc);
                var model = OdataConfiguration.GetEdmModel(provider);
                routeBuilder.MapODataServiceRoute("odata/pediakids", "odata/pediakids", model);
                routeBuilder.MapODataServiceRoute("auth", "auth", model);
                routeBuilder.EnableDependencyInjection();
            });
        }
    }
}
