using Ms.Cross.Security.Models;
using Ms.Cross.Security.Services.Mapper;
using Ms.Pediakids.Core.DTO;

namespace Ms.Pediakids.Core.Services.Mapper
{
    public class ApplicationUserMapper: MapperGeneric<ApplicationUser,ApplicationUserDTO>
    {
        #region Atributes
        #endregion

        #region Constructor
        public ApplicationUserMapper()
        {
        }
        #endregion

        #region Methods
        #endregion
    }
}