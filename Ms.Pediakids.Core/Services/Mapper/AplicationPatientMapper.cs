using Ms.Cross.Security.Services.Mapper;
using Ms.Pediakids.Core.DTO;
using Ms.Pediakids.Core.Models;

namespace Ms.Pediakids.Core.Services.Mapper
{
    public class ApplicationPatientMapper: MapperGeneric<Patient, PatientDTO>
    {
        #region Atributes
        #endregion

        #region Constructor
        public ApplicationPatientMapper()
        {
        }
        #endregion

        #region Methods
        #endregion
    }
}