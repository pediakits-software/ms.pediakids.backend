using System.Runtime.Serialization;

namespace Ms.Pediakids.Core.Services.Utils
{
    public enum ExeptionUserProfileEnum
    {
         [EnumMember(Value = "Username does not exist")] USER_NOT_EXIST, 
         [EnumMember(Value = "Current password does not match")] PASSWORD_NOT_MATCH, 
         [EnumMember(Value = "could not remove password")] COULD_NOT_REMOVE_PASSWORD, 
    }
}