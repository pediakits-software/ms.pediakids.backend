using System.Runtime.Serialization;

namespace Ms.Pediakids.Core.Services.Utils
{
    public enum  ExceptionPatientEnum
    {
        [EnumMember(Value = "There is already a patient with the identification number")]
        PATIENT_ALREADY_EXIST,
        [EnumMember(Value = "Invalid date of birth")]
        INVALID_DATE_BIRTH,
        [EnumMember(Value = "Invalid delete, this record already have a history of medical consultations")]
        PATIENT_ALREADY_MEDICAL_CONSULTATION      
    }

    public enum  ExceptionEncryptorEnum
    {
        [EnumMember(Value = "Invalid encryptor password")]
        INVALID_ENCRYPTOR_PASSWORD, 
    }
}