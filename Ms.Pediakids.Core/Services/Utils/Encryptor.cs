using System.Text;  
using System.Security.Cryptography;  
using System;
using System.IO;
using Ms.Cross.Security.Utils.api;
using System.Net;

namespace Ms.Pediakids.Core.Services.Utils
{
   public static class Encryptor  
  {  

    public static string IV = @"!QAZ2WSX#EDC4RFV";
    public static string Key = @"5TGB&YHN7UJM(IK<5TGB&YHN7UJM(IK<";
    public static string Encrypt(string dectypted)
    {

        byte[] textbytes = ASCIIEncoding.ASCII.GetBytes(dectypted);
        AesCryptoServiceProvider encdec = new AesCryptoServiceProvider();
        encdec.BlockSize = 128;
        encdec.KeySize = 256;
        encdec.Key = ASCIIEncoding.ASCII.GetBytes(Key);
        encdec.IV = ASCIIEncoding.ASCII.GetBytes(IV);
        encdec.Padding = PaddingMode.PKCS7;
        encdec.Mode = CipherMode.CBC;

        ICryptoTransform icrypt = encdec.CreateEncryptor(encdec.Key, encdec.IV);

        byte[] enc = icrypt.TransformFinalBlock(textbytes, 0, textbytes.Length);
        icrypt.Dispose();

        var dataEncryter= Convert.ToBase64String(enc);
        string converted = dataEncryter.Replace("/", "&&");
        return converted;
    }


    public static  ApiResponse  Decrypt(string dectypted)
    {

      try{
        //byte[] textbytes = ASCIIEncoding.ASCII.GetBytes(dectypted);
        var textbytes = Convert.FromBase64String(dectypted);

        AesCryptoServiceProvider encdec = new AesCryptoServiceProvider();
        encdec.BlockSize = 128;
        encdec.KeySize = 256;
        encdec.Key = ASCIIEncoding.ASCII.GetBytes(Key);
        encdec.IV = ASCIIEncoding.ASCII.GetBytes(IV);
        encdec.Padding = PaddingMode.PKCS7;
        encdec.Mode = CipherMode.CBC;

        ICryptoTransform decryptor = encdec.CreateDecryptor(encdec.Key, encdec.IV);

        using (MemoryStream memoryStream = new MemoryStream(textbytes))  
        {  
            using (CryptoStream cryptoStream = new CryptoStream((Stream)memoryStream, decryptor, CryptoStreamMode.Read))  
            {  
                using (StreamReader streamReader = new StreamReader((Stream)cryptoStream))  
                {  
                  return ApiResponseResult.OK(streamReader.ReadToEnd());
                }  
            }  
        }  
      }catch(Exception ex){
        return ApiResponseResult.ERROR_EXCEPTION(new Exception(), message : "Invalid encryptor password");
      }
    }


    public static string MD5Hash(string text)  
    {  
      MD5 md5 = new MD5CryptoServiceProvider();  

      //compute hash from the bytes of text  
      md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(text));  
  
      //get hash result after compute it  
      byte[] result = md5.Hash;  

      StringBuilder strBuilder = new StringBuilder();  
      for (int i = 0; i < result.Length; i++)  
      {  
        //change it into 2 hexadecimal digits  
        //for each byte  
        strBuilder.Append(result[i].ToString("x2"));  
      }  

      return strBuilder.ToString();  
    }  
  }  
}