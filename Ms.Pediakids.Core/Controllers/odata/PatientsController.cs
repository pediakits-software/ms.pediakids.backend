using System;
using System.Threading.Tasks;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Routing;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Ms.Pediakids.Core.Data.Migrations;
using Ms.Pediakids.Core.DTO;
using Ms.Pediakids.Core.Models;
using Ms.Pediakids.Core.Services.Mapper;
using System.Collections.Generic;
using System.Linq;

using System.Net;
using System.Data;
using Ms.Pediakids.Core.Data;
using Ms.Cross.Security.Utils.api;
using Ms.Cross.Security.Utils.constant;
using Ms.Pediakids.Core.Services.Utils;
using Ms.Cross.Security.Utils.api;
namespace Ms.Pediakids.Core.Controllers.odata
{
    [ODataRoutePrefix("odata/pediakids/Patients")]
    [Route("mvc/odata/pediakids/Patients")]
    public partial class PatientsController : ODataController
    {
        private AppDohDbContext context;

        public PatientsController(AppDohDbContext context)
        {
            this.context = context;
        }
        // GET /odata/doh/Patients
        [EnableQuery(MaxExpansionDepth = 10, MaxNodeCount = 1000)]
        [HttpGet]
        public IEnumerable<Patient> GetPatients()
        {
            var items = this.context.Patients.AsQueryable<Patient>();
            this.OnPatientsRead(ref items);

            return items;
        }

        partial void OnPatientsRead(ref IQueryable<Patient> items);

        [EnableQuery(MaxExpansionDepth = 10, MaxNodeCount = 1000)]
        [HttpGet("{Id}")]
        public SingleResult<Patient> GetPatient(int key)
        {
            var items = this.context.Patients.Where(i => i.Id == key);
            return SingleResult.Create(items);
        }
        partial void OnPatientDeleted(Patient item);

        [HttpDelete("{Id}")]
        public IActionResult DeletePatient(int key)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                    
                bool haveMedicalConsultation = context.MedicalConsultations.Any(x=> x.PatientId == key);
                if(haveMedicalConsultation)
                {
                    return  ApiActionResult.ErrorApi<ExceptionPatientEnum>(ExceptionPatientEnum.PATIENT_ALREADY_MEDICAL_CONSULTATION, message:"El paciente ya tiene consultas medicas y no se puede eliminar");
                }
                
                var items = this.context.Patients
                    .Where(i => i.Id == key)
                    .AsQueryable();

                items = EntityPatch.ApplyTo<Patient>(Request, items);

                var item = items.FirstOrDefault();

                if (item == null)
                {
                    return StatusCode((int)HttpStatusCode.PreconditionFailed);
                }

                this.OnPatientDeleted(item);
                this.context.Patients.Remove(item);
                this.context.SaveChanges();

                return new NoContentResult();
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return BadRequest(ModelState);
            }
        }

        partial void OnPatientUpdated(Patient item);

        [HttpPut("{Id}")]
        [EnableQuery(MaxExpansionDepth = 10, MaxNodeCount = 1000)]
        public IActionResult PutPatient(int key, [FromBody] Patient newItem)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                if (newItem.FechaNacimiento >= DateTime.Now)
                {
                    return  ApiActionResult.ErrorApi<ExceptionPatientEnum>(ExceptionPatientEnum.INVALID_DATE_BIRTH, message:"La fecha de nacimiento no puede ser igual o mayor a la actual");
                }

                var items = this.context.Patients
                    .Where(i => i.Id == key)
                    .AsQueryable();

                items = EntityPatch.ApplyTo<Patient>(Request, items);

                var item = items.FirstOrDefault();

                if (item == null)
                {
                    return StatusCode((int)HttpStatusCode.PreconditionFailed);
                }

                newItem.NombreCompleto = string.Concat(newItem.Nombres,' ', newItem.Apellidos);
                this.OnPatientUpdated(newItem);
                this.context.Patients.Update(newItem);
                this.context.SaveChanges();

                var itemToReturn = this.context.Patients.Where(i => i.Id == key);
                //Request.QueryString = Request.QueryString.Add("$expand", "Empresa");
                return new ObjectResult(SingleResult.Create(itemToReturn));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return BadRequest(ModelState);
            }
        }

        [HttpPatch("{Id}")]
        [EnableQuery(MaxExpansionDepth = 10, MaxNodeCount = 1000)]
        public IActionResult PatchPatient(int key, [FromBody] Delta<Patient> patch)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                var items = this.context.Patients.Where(i => i.Id == key);

                items = EntityPatch.ApplyTo<Patient>(Request, items);

                var item = items.FirstOrDefault();

                if (item == null)
                {
                    return StatusCode((int)HttpStatusCode.PreconditionFailed);
                }

                item.NombreCompleto = string.Concat(item.Nombres,' ',item.Apellidos);
                patch.Patch(item);

                this.OnPatientUpdated(item);
                this.context.Patients.Update(item);
                this.context.SaveChanges();

                var itemToReturn = this.context.Patients.Where(i => i.Id == key);
                //Request.QueryString = Request.QueryString.Add("$expand", "Empresa");
                return new ObjectResult(SingleResult.Create(itemToReturn));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return BadRequest(ModelState);
            }
        }

        partial void OnPatientCreated(Patient item);

        [HttpPost]
        [EnableQuery(MaxExpansionDepth = 10, MaxNodeCount = 1000)]
        public IActionResult Post([FromBody] Patient item)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                if (item == null)
                {
                    return BadRequest();
                }
                bool identity = context.Patients.Any(x => x.NumeroIdentificacion == item.NumeroIdentificacion);
                if (identity)
                {
                    return  ApiActionResult.ErrorApi<ExceptionPatientEnum>(ExceptionPatientEnum.PATIENT_ALREADY_EXIST, message:string.Format("El paciente {0} {1} con número de identificación {2}, ya existe en el sistema",
                                item.Nombres, item.Apellidos, item.NumeroIdentificacion));
                }

                item.NombreCompleto = string.Concat(item.Nombres,' ',item.Apellidos);
                if (item.FechaNacimiento >= DateTime.Now)
                {
                    return  ApiActionResult.ErrorApi<ExceptionPatientEnum>(ExceptionPatientEnum.INVALID_DATE_BIRTH, message:"La fecha de nacimiento no puede ser igual o mayor a la actual");
                }

                this.OnPatientCreated(item);
                this.context.Patients.Add(item);
                this.context.SaveChanges();
                var key = item.Id;
                var itemToReturn = this.context.Patients.Where(i => i.Id == key);
                //Request.QueryString = Request.QueryString.Add("$expand", "Empresa");
                return new ObjectResult(SingleResult.Create(itemToReturn))
                {
                    StatusCode = 201
                };
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return BadRequest(ModelState);
            }
        }
    }
}

/* 
namespace Ms.Pediakids.Core.Controllers.odata
{
    [ODataRoutePrefix("odata/pediakids/Patient")]
    [Route("api/pediakids/Patient")]
    public partial class PatientController : ODataController
    {
        private readonly AppDohDbContext context;
        private readonly ApplicationPatientMapper _ApplicationPatientMapper;

        public PatientController(AppDohDbContext context, ApplicationPatientMapper applicationPatientMapper)
        {
            this.context = context;
            _ApplicationPatientMapper = applicationPatientMapper;
        }

        [EnableQuery]
        [HttpGet]
        public IActionResult Get()
        {
            return Ok(context.Patients.AsQueryable());
        }

        [EnableQuery]
        [HttpGet("{id:int}", Name = "getPatient")]
        public async Task<ActionResult<Patient>> Get(int id)
        {
            var entitie = await context.Patients.FirstOrDefaultAsync(x => x.Id == id);
            if (entitie == null)
            {
                return NotFound();
            }
            return entitie;
        }

        [HttpPost]
        [EnableQuery]
        public async Task<ActionResult> Post([FromBody] PatientDTO patientDTO)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                if (patientDTO == null)
                {
                    return BadRequest();
                }

                var identity = await context.Patients.FirstOrDefaultAsync(x => x.NumeroIdentificacion == patientDTO.NumeroIdentificacion);
                if (identity != null)
                {
                    return BadRequest(
                        string.Format("El paciente {0} {1} con número de identificación {2}, ya existe en el sistema",
                         patientDTO.Nombres, patientDTO.Apellidos, patientDTO.NumeroIdentificacion)
                        );
                }

                if (patientDTO.FechaNacimiento >= DateTime.Now)
                {
                    return BadRequest("La fecha de nacimiento no puede ser igual o mayor a la actual");
                }
                var result = _ApplicationPatientMapper.toEntity(patientDTO);
                this.context.Patients.Add(result);
                await this.context.SaveChangesAsync();
                return new CreatedAtRouteResult("getPatient", new { id = result.Id }, result);
            }
            catch (System.Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return BadRequest(ModelState);
            }
        }
        
        [HttpPut("{id}")]
        public async Task<ActionResult> Put(int id, [FromBody] PatientDTO patientDTO)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                var patientDb = await context.Patients.FirstOrDefaultAsync(x => x.Id == id);
                if (patientDb == null)
                {
                    return NotFound();
                }

                if (patientDTO.FechaNacimiento >= DateTime.Now)
                {
                    return BadRequest("La fecha de nacimiento no puede ser igual o mayor a la actual");
                }
                patientDb = _ApplicationPatientMapper.toEntity(patientDTO);
                context.Patients.Update(patientDb);
                await context.SaveChangesAsync();
                return NoContent();
            }
            catch (System.Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return BadRequest(ModelState);
            }
        }

         [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            bool exists = await context.Patients.AnyAsync(x=> x.Id == id);
            if(!exists)
            {
                return NotFound();
            }

            bool haveMedicalConsultation = await context.medicalConsultations.AnyAsync(x=> x.PatientId == id);
            if(haveMedicalConsultation)
            {
                return BadRequest("El paciente ya tiene consultas medicas y no se puede eliminar");
            }
            context.Remove(new Patient(){Id = id});
            await context.SaveChangesAsync();
            return NoContent();
        }


    }

} */