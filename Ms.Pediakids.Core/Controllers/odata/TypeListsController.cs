using System;
using System.Net;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Routing;
using Ms.Pediakids.Core.Data.Migrations;
using Ms.Pediakids.Core.Models;
using Ms.Pediakids.Core.Data;


namespace Ms.Pediakids.Core.Controllers.odata
{
   [ODataRoutePrefix("odata/pediakids/TypeLists")]
  [Route("mvc/odata/pediakids/TypeLists")]
  public partial class TypeListsController : ODataController
  {
    private AppDohDbContext context;

    public TypeListsController(AppDohDbContext context)
    {
      this.context = context;
    }
    // GET /odata/doh/TypeLists
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    [HttpGet]
    public IEnumerable<TypeList> GetTypeLists()
    {
      var items = this.context.TypeLists.AsQueryable<TypeList>();
      this.OnTypeListsRead(ref items);

      return items;
    }

    partial void OnTypeListsRead(ref IQueryable<TypeList> items);

    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    [HttpGet("{Id}")]
    public SingleResult<TypeList> GetTypeList(int key)
    {
        var items = this.context.TypeLists.Where(i=>i.Id == key);
        return SingleResult.Create(items);
    }
    partial void OnTypeListDeleted(TypeList item);

    [HttpDelete("{Id}")]
    public IActionResult DeleteTypeList(int key)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var items = this.context.TypeLists
                .Where(i => i.Id == key)
                .AsQueryable();

            items = EntityPatch.ApplyTo<TypeList>(Request, items);

            var item = items.FirstOrDefault();

            if (item == null)
            {
                return StatusCode((int)HttpStatusCode.PreconditionFailed);
            }

            this.OnTypeListDeleted(item);
            this.context.TypeLists.Remove(item);
            this.context.SaveChanges();

            return new NoContentResult();
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }

    partial void OnTypeListUpdated(TypeList item);

    [HttpPut("{Id}")]
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    public IActionResult PutTypeList(int key,[FromBody]TypeList newItem)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var items = this.context.TypeLists
                .Where(i => i.Id == key)
                .AsQueryable();

            items = EntityPatch.ApplyTo<TypeList>(Request, items);

            var item = items.FirstOrDefault();

            if (item == null)
            {
                return StatusCode((int)HttpStatusCode.PreconditionFailed);
            }

            this.OnTypeListUpdated(newItem);
            this.context.TypeLists.Update(newItem);
            this.context.SaveChanges();

            var itemToReturn = this.context.TypeLists.Where(i => i.Id == key);
            //Request.QueryString = Request.QueryString.Add("$expand", "Empresa");
            return new ObjectResult(SingleResult.Create(itemToReturn));
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }

    [HttpPatch("{Id}")]
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    public IActionResult PatchTypeList(int key, [FromBody]Delta<TypeList> patch)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var items = this.context.TypeLists.Where(i => i.Id == key);

            items = EntityPatch.ApplyTo<TypeList>(Request, items);

            var item = items.FirstOrDefault();

            if (item == null)
            {
                return StatusCode((int)HttpStatusCode.PreconditionFailed);
            }

            patch.Patch(item);

            this.OnTypeListUpdated(item);
            this.context.TypeLists.Update(item);
            this.context.SaveChanges();

            var itemToReturn = this.context.TypeLists.Where(i => i.Id == key);
            //Request.QueryString = Request.QueryString.Add("$expand", "Empresa");
            return new ObjectResult(SingleResult.Create(itemToReturn));
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }

    partial void OnTypeListCreated(TypeList item);

    [HttpPost]
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    public IActionResult Post([FromBody] TypeList item)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (item == null)
            {
                return BadRequest();
            }

            this.OnTypeListCreated(item);
            this.context.TypeLists.Add(item);
            this.context.SaveChanges();
            var key = item.Id;
            var itemToReturn = this.context.TypeLists.Where(i => i.Id == key);
            //Request.QueryString = Request.QueryString.Add("$expand", "Empresa");
            return new ObjectResult(SingleResult.Create(itemToReturn))
            {
                StatusCode = 201
            };
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }
  }
}