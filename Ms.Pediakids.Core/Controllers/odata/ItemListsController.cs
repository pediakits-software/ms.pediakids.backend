using System;
using System.Net;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Routing;
using Ms.Pediakids.Core.Data.Migrations;
using Ms.Pediakids.Core.Models;
using Ms.Pediakids.Core.Data;

namespace Ms.Pediakids.Core.Controllers.odata
{
    [ODataRoutePrefix("odata/pediakids/ItemLists")]
  [Route("mvc/odata/pediakids/ItemLists")]
  public partial class ItemListsController : ODataController
  {
    private AppDohDbContext context;

    public ItemListsController(AppDohDbContext context)
    {
      this.context = context;
    }
    // GET /odata/doh/ItemLists
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    [HttpGet]
    public IEnumerable<ItemList> GetItemLists()
    {
      var items = this.context.ItemLists.AsQueryable<ItemList>();
      this.OnItemListsRead(ref items);

      return items;
    }

    partial void OnItemListsRead(ref IQueryable<ItemList> items);

    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    [HttpGet("{Id}")]
    public SingleResult<ItemList> GetItemList(int key)
    {
        var items = this.context.ItemLists.Where(i=>i.Id == key);
        return SingleResult.Create(items);
    }
    partial void OnItemListDeleted(ItemList item);

    [HttpDelete("{Id}")]
    public IActionResult DeleteItemList(int key)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var items = this.context.ItemLists
                .Where(i => i.Id == key)
                .AsQueryable();

            items = EntityPatch.ApplyTo<ItemList>(Request, items);

            var item = items.FirstOrDefault();

            if (item == null)
            {
                return StatusCode((int)HttpStatusCode.PreconditionFailed);
            }

            this.OnItemListDeleted(item);
            this.context.ItemLists.Remove(item);
            this.context.SaveChanges();

            return new NoContentResult();
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }

    partial void OnItemListUpdated(ItemList item);

    [HttpPut("{Id}")]
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    public IActionResult PutItemList(int key,[FromBody]ItemList newItem)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var items = this.context.ItemLists
                .Where(i => i.Id == key)
                .AsQueryable();

            items = EntityPatch.ApplyTo<ItemList>(Request, items);

            var item = items.FirstOrDefault();

            if (item == null)
            {
                return StatusCode((int)HttpStatusCode.PreconditionFailed);
            }

            this.OnItemListUpdated(newItem);
            this.context.ItemLists.Update(newItem);
            this.context.SaveChanges();

            var itemToReturn = this.context.ItemLists.Where(i => i.Id == key);
            //Request.QueryString = Request.QueryString.Add("$expand", "Empresa");
            return new ObjectResult(SingleResult.Create(itemToReturn));
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }

    [HttpPatch("{Id}")]
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    public IActionResult PatchItemList(int key, [FromBody]Delta<ItemList> patch)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var items = this.context.ItemLists.Where(i => i.Id == key);

            items = EntityPatch.ApplyTo<ItemList>(Request, items);

            var item = items.FirstOrDefault();

            if (item == null)
            {
                return StatusCode((int)HttpStatusCode.PreconditionFailed);
            }

            patch.Patch(item);

            this.OnItemListUpdated(item);
            this.context.ItemLists.Update(item);
            this.context.SaveChanges();

            var itemToReturn = this.context.ItemLists.Where(i => i.Id == key);
            //Request.QueryString = Request.QueryString.Add("$expand", "Empresa");
            return new ObjectResult(SingleResult.Create(itemToReturn));
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }

    partial void OnItemListCreated(ItemList item);

    [HttpPost]
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    public IActionResult Post([FromBody] ItemList item)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (item == null)
            {
                return BadRequest();
            }

            this.OnItemListCreated(item);
            this.context.ItemLists.Add(item);
            this.context.SaveChanges();
            var key = item.Id;
            var itemToReturn = this.context.ItemLists.Where(i => i.Id == key);
            //Request.QueryString = Request.QueryString.Add("$expand", "Empresa");
            return new ObjectResult(SingleResult.Create(itemToReturn))
            {
                StatusCode = 201
            };
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }
  }
}