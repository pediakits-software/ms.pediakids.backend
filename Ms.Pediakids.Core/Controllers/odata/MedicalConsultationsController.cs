using System;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Routing;
using Microsoft.AspNetCore.Mvc;
using Ms.Pediakids.Core.Data.Migrations;
using Ms.Pediakids.Core.Models;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Data;
using Ms.Pediakids.Core.Data;

namespace Ms.Pediakids.Core.Controllers.odata
{
    [ODataRoutePrefix("odata/pediakids/MedicalConsultations")]
  [Route("mvc/odata/pediakids/MedicalConsultations")]
  public partial class MedicalConsultationsController : ODataController
  {
    private AppDohDbContext context;

    public MedicalConsultationsController(AppDohDbContext context)
    {
      this.context = context;
    }
    // GET /odata/doh/MedicalConsultations
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    [HttpGet]
    public IEnumerable<MedicalConsultation> GetMedicalConsultations()
    {
      var items = this.context.MedicalConsultations.AsQueryable<MedicalConsultation>();
      this.OnMedicalConsultationsRead(ref items);

      return items;
    }

    partial void OnMedicalConsultationsRead(ref IQueryable<MedicalConsultation> items);

    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    [HttpGet("{Id}")]
    public SingleResult<MedicalConsultation> GetMedicalConsultation(int key)
    {
        var items = this.context.MedicalConsultations.Where(i=>i.Id == key);
        return SingleResult.Create(items);
    }
    partial void OnMedicalConsultationDeleted(MedicalConsultation item);

    [HttpDelete("{Id}")]
    public IActionResult DeleteMedicalConsultation(int key)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var items = this.context.MedicalConsultations
                .Where(i => i.Id == key)
                .AsQueryable();

            items = EntityPatch.ApplyTo<MedicalConsultation>(Request, items);

            var item = items.FirstOrDefault();

            if (item == null)
            {
                return StatusCode((int)HttpStatusCode.PreconditionFailed);
            }

            this.OnMedicalConsultationDeleted(item);
            this.context.MedicalConsultations.Remove(item);
            this.context.SaveChanges();

            return new NoContentResult();
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }

    partial void OnMedicalConsultationUpdated(MedicalConsultation item);

    [HttpPut("{Id}")]
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    public IActionResult PutMedicalConsultation(int key,[FromBody]MedicalConsultation newItem)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var items = this.context.MedicalConsultations
                .Where(i => i.Id == key)
                .AsQueryable();

            items = EntityPatch.ApplyTo<MedicalConsultation>(Request, items);

            var item = items.FirstOrDefault();

            if (item == null)
            {
                return StatusCode((int)HttpStatusCode.PreconditionFailed);
            }

            this.OnMedicalConsultationUpdated(newItem);
            this.context.MedicalConsultations.Update(newItem);
            this.context.SaveChanges();

            var itemToReturn = this.context.MedicalConsultations.Where(i => i.Id == key);
            //Request.QueryString = Request.QueryString.Add("$expand", "Empresa");
            return new ObjectResult(SingleResult.Create(itemToReturn));
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }

    [HttpPatch("{Id}")]
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    public IActionResult PatchMedicalConsultation(int key, [FromBody]Delta<MedicalConsultation> patch)
    {
        try
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var items = this.context.MedicalConsultations.Where(i => i.Id == key);

            items = EntityPatch.ApplyTo<MedicalConsultation>(Request, items);

            var item = items.FirstOrDefault();

            if (item == null)
            {
                return StatusCode((int)HttpStatusCode.PreconditionFailed);
            }

            patch.Patch(item);

            this.OnMedicalConsultationUpdated(item);
            this.context.MedicalConsultations.Update(item);
            this.context.SaveChanges();

            var itemToReturn = this.context.MedicalConsultations.Where(i => i.Id == key);
            //Request.QueryString = Request.QueryString.Add("$expand", "Empresa");
            return new ObjectResult(SingleResult.Create(itemToReturn));
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }

    partial void OnMedicalConsultationCreated(MedicalConsultation item);

    [HttpPost]
    [EnableQuery(MaxExpansionDepth=10,MaxNodeCount=1000)]
    public IActionResult Post([FromBody] MedicalConsultation item)
    {
        try
        {   
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (item == null)
            {
                return BadRequest();
            }

            this.OnMedicalConsultationCreated(item);
            this.context.MedicalConsultations.Add(item);
            this.context.SaveChanges();
            var key = item.Id;
            var itemToReturn = this.context.MedicalConsultations.Where(i => i.Id == key);
            //Request.QueryString = Request.QueryString.Add("$expand", "Empresa");
            return new ObjectResult(SingleResult.Create(itemToReturn))
            {
                StatusCode = 201
            };
        }
        catch(Exception ex)
        {
            ModelState.AddModelError("", ex.Message);
            return BadRequest(ModelState);
        }
    }
  }
}
