using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Ms.Pediakids.Core.Controllers
{
    public partial class HomeController : Controller
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        public HomeController(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }
        public IActionResult Index()
        {
            string host = _httpContextAccessor.HttpContext.Request.Host.Value;
            if(!host.Contains(":5000")){
                host=$"{host}:5000";
            }
            ViewBag.Home = new Home{
                domain= host
            };
            return View();  
        }
    }
}