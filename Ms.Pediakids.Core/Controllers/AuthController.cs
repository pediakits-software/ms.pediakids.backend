using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Ms.Cross.Jwt.Src;
using Ms.Cross.Security.DTO;
using Ms.Cross.Security.Helper;
using Ms.Cross.Security.Models;
using Ms.Cross.Security.Utils.api;
using Ms.Cross.Security.Utils.constant;
using Ms.Pediakids.Core.DTO;
using Ms.Pediakids.Core.Services.Mapper;
using Ms.Pediakids.Core.Services.Utils;

namespace Ms.Pediakids.Core.Controllers
{
    [Route("/auth/[action]")]
    public class AuthController : Controller
    {
        private readonly JwtOptions _jwtOption;
        private readonly ILoginHelper _ILoginHelper;
        private readonly IUserHelper _userHelper;
        private readonly ApplicationUserMapper _ApplicationUserMapper;

        public AuthController(IOptionsSnapshot<JwtOptions> jwtOption, ILoginHelper iLoginHelper, IUserHelper userHelper, ApplicationUserMapper applicationUserMapper)
        {
            _jwtOption = jwtOption.Value;
            _ILoginHelper = iLoginHelper;
            _userHelper = userHelper;
            _ApplicationUserMapper = applicationUserMapper;
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Login([FromBody] LoginRequestDTO request)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                var passwordDecrypt = Encryptor.Decrypt(request.Password);
                if (!passwordDecrypt.IsSuccess)
                {
                    return ApiActionResult.ErrorApi<ExceptionEncryptorEnum>(ExceptionEncryptorEnum.INVALID_ENCRYPTOR_PASSWORD);
                }

                var responseLogin = await this._ILoginHelper.CheckLoginAsync(request, passwordDecrypt.Result.ToString());
                if (!responseLogin.IsSuccess)
                {
                    return ApiActionResult.ErrorApi(apiException: (ApiException)responseLogin.Result);
                }
                var user = (ApplicationUser)responseLogin.Result;
                var principal = await this._ILoginHelper.CreateUserPrincipalAsync(user);
                var tokenResponse = JwtToken.createToken(principal.Claims, _jwtOption);
                Response.Headers.Add("Access-Control-Expose-Headers", "access_token");
                Response.Headers.Add("Access-Control-Expose-Headers-Expire", "expires_in");
                Response.Headers.Add("access_token", tokenResponse.access_token);
                Response.Headers.Add("expires_in", tokenResponse.expires_in.ToString());
                return ApiActionResult.OkApi<ApplicationUserDTO>(_ApplicationUserMapper.toDTO(user));
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return BadRequest(ModelState);
            }
        }

        [HttpPost]
        public async Task<IActionResult> Register([FromBody] UserIdentityRequestDTO userRequest)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                var response = await this._userHelper.CreateUserAccount(userRequest, EmailConfirmed: true);
                if (!response.IsSuccess)
                {
                    return ApiActionResult.ErrorApi(apiException: (ApiException)response.Result);
                }
                var user = (ApplicationUser)response.Result;
                return ApiActionResult.OkApi<ApplicationUserDTO>(_ApplicationUserMapper.toDTO(user),
                statusCode: HttpStatusCode.Created);
                //return Created($"auth/Users('{user.Id}')", user);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return BadRequest(ModelState);
            }
        }

        [HttpPut]
        public async Task<IActionResult> UpdatePassword([FromBody] UpdatePasswordDTO user)
        {
            try
            {
                string idUser = Encryptor.Decrypt(user.IdUser).Result.ToString();
                string currentPassword = Encryptor.Decrypt(user.CurrentPassword).Result.ToString();
                string newPassword = Encryptor.Decrypt(user.NewPassword).Result.ToString();

                var existUser = await _userHelper.GetUserByIdAsync(idUser);
                if (existUser == null)
                {
                    return ApiActionResult.ErrorApi<ExeptionUserProfileEnum>(ExeptionUserProfileEnum.USER_NOT_EXIST, message: "El usuario no existe en el sistema");
                }

                var userCheck = new LoginRequestDTO()
                {
                    Password = currentPassword,
                    Username = existUser.Email,
                    Rememberme = true
                };
                var responseLogin = await this._ILoginHelper.CheckLoginAsync(userCheck, currentPassword);
                if (!responseLogin.IsSuccess)
                {
                    return ApiActionResult.ErrorApi<ExeptionUserProfileEnum>(ExeptionUserProfileEnum.PASSWORD_NOT_MATCH, message: "La contraseña actual no coincide");
                }

                var removePassword = await _userHelper.RemovePasswordUserAsync(existUser);
                if (!removePassword.Succeeded)
                {
                    return ApiActionResult.ErrorApi<ExeptionUserProfileEnum>(ExeptionUserProfileEnum.COULD_NOT_REMOVE_PASSWORD, message: "No fue posible remover la contraseña");
                }

                var update = await _userHelper.AddPasswordUserAsync(existUser, newPassword);
                if (!update.Succeeded)
                {
                    return ApiActionResult.ErrorApi(apiException: (ApiException)update.Errors);
                }

                return ApiActionResult.OkApi<Boolean>(true, statusCode: HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return BadRequest(ModelState);
            }
        }
    }
}