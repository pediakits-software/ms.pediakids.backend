**1. Run Migrations Security.**   
- create bd **_doh_**
- delete all files folder **_AuthMigrations_** from ___Data___ 
- run for create migrations **_dotnet ef migrations add SecurityIdentityMigration --output-dir "Data/AuthMigrations"  --context SecurityDbContext_**
- update database **_dotnet ef database update  --context SecurityDbContext_**


**2. Run Migrations App.**   
- create bd **_doh_**
- delete all files folder **_AppMigrations_** from ___Data___ 
- run for create migrations **_dotnet ef migrations add AppInitialMigration --output-dir "Data/AppMigrations"  --context AppDohDbContext_**
- update database **_dotnet ef database update  --context AppDohDbContext_**

**2. Run  App.**  
- run command **_dotnet run_**
