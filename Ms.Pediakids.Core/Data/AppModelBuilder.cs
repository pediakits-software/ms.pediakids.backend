using Microsoft.EntityFrameworkCore;
using Ms.Pediakids.Core.Models;

namespace Ms.Pediakids.Core.Data
{
    public static class AppModelBuilder
    {
        public static ModelBuilder OnModelCreating(ModelBuilder builder)
        {

            #region ItemList
            builder.Entity<ItemList>()
                  .HasOne(i => i.TypeList)
                  .WithMany(i => i.ItemsList)
                  .HasForeignKey(i => i.TypeListId)
                  .HasPrincipalKey(i => i.Id);

            builder.Entity<ItemList>()
                  .HasOne(i => i.ItemListOwner)
                  .WithMany(i => i.ItemListChilds)
                  .HasForeignKey(i => i.Owner_Id)
                  .HasPrincipalKey(i => i.Id);

            builder.Entity<ItemList>()
                  .Property(p => p.Active)
                  .HasDefaultValueSql("true"); 

            #endregion

            #region Patient
            builder.Entity<Patient>()
                  .HasOne(i => i.TipoIdentificacion)
                  .WithMany(i => i.Patients)
                  .HasForeignKey(i => i.TipoIdentificacionId)
                  .HasPrincipalKey(i => i.Id);
 builder.Entity<Patient>()
                  .HasOne(i => i.Genero)
                  .WithMany(i => i.PatientGeneros)
                  .HasForeignKey(i => i.GeneroId)
                  .HasPrincipalKey(i => i.Id);
            builder.Entity<MedicalConsultation>()
                  .HasOne(i => i.Patient)
                  .WithMany(i => i.MedicalConsultations)
                  .HasForeignKey(i => i.PatientId)
                  .HasPrincipalKey(i => i.Id);


            #endregion

            #region DEFINICION DE INDEX
            
            builder.Entity<ItemList>()
                  .HasIndex(p => new {p.Code})
                  .IsUnique();
            #endregion
            return builder;
        }
    }
}