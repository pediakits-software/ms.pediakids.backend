using System;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Ms.Cross.Security.Utils;
using Ms.Cross.Security.Utils.extencions;
using Ms.Pediakids.Core.Models;

namespace Ms.Pediakids.Core.Data.Migrations
{
    public class AppDohDbContext:DbContext
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public AppDohDbContext(IHttpContextAccessor httpAccessor, DbContextOptions<AppDohDbContext> options):base(options)
        {
            _httpContextAccessor = httpAccessor;
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            # region Call Model Builder AppEntities
            builder = AppModelBuilder.OnModelCreating(builder);
            # endregion
            builder.HasDefaultSchema("public"); // Esquema por defecto
        }

        #region Audit Tables
        // ==============================================================================
        public override int SaveChanges()
        {
            Console.WriteLine("==========================SaveChangesAsync======================================");
            // get entries that are being Added or Updated
            // get entries that are being Added or Updated
            // get entries that are being Added or Updated
            var modifiedEntries = ChangeTracker.Entries()
                    .Where(x => (x.State == EntityState.Added || x.State == EntityState.Modified));
            var userNameSystem = "";
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows)){
                //userNameSystem= System.Security.Principal.WindowsIdentity.GetCurrent().Name;
                userNameSystem= string.Format("{0}- {1}-{2} DOMAIN: ({3})", Environment.UserName, Environment.OSVersion, Environment.MachineName, Environment.UserDomainName);
            }
            if (!RuntimeInformation.IsOSPlatform(OSPlatform.Windows)){
                userNameSystem= string.Format("{0}- {1}-{2} DOMAIN: ({3})", Environment.UserName, Environment.OSVersion, Environment.MachineName, Environment.UserDomainName);
            }            
            
            // var userIdentity = _httpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier);
            var userIdentity = "";
            if(_httpContextAccessor.HttpContext !=null && _httpContextAccessor.HttpContext.User!=null) {
                userIdentity = _httpContextAccessor.HttpContext.User.GetLoggedInUserId<string>();
            }
            var now = Util.getDateTimeNow();
            foreach (var entry in modifiedEntries)
            {
                // var entity = entry.Entity as AuditableEntity;
                var verificatePropertyAudityExist = Util.HasProperty(entry.Entity, "CreatedDate");
                if (verificatePropertyAudityExist)
                {
                    if (entry.State == EntityState.Added)
                    {
                        
                        entry.Entity.GetType().GetProperty("CreatedBySystem").SetValue(entry.Entity, String.IsNullOrEmpty(userNameSystem)  ? "unknown":userNameSystem, null);
                        entry.Entity.GetType().GetProperty("CreatedBy").SetValue(entry.Entity, String.IsNullOrEmpty(userIdentity)  ? "unknown":userIdentity, null);
                        entry.Entity.GetType().GetProperty("CreatedDate").SetValue(entry.Entity, now, null);
                        /* entity.CreatedBySystem = userNameSystem ?? "unknown";
                        entity.CreatedByIdentity = userIdentity ?? "unknown";
                        entity.CreatedDate = now; */
                    }
                    else
                    {
                        /* entity.UpdateBySystem = userNameSystem ?? "unknown";
                        entity.UpdatedByIdentity = userIdentity ?? "unknown";
                        entity.UpdatedDate = now; */
                        entry.Entity.GetType().GetProperty("LastModifiedBySystem").SetValue(entry.Entity, String.IsNullOrEmpty(userNameSystem)  ? "unknown":userNameSystem, null);
                        entry.Entity.GetType().GetProperty("LastModifiedBy").SetValue(entry.Entity,String.IsNullOrEmpty(userIdentity)  ? "unknown":userIdentity, null);
                        entry.Entity.GetType().GetProperty("LastModifiedDate").SetValue(entry.Entity, now, null);
                    }
                }
            }
            return base.SaveChanges();
            // return _helper.SaveChanges(_auditContext, () => base.SaveChanges());
        }

        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            Console.WriteLine("==========================SaveChangesAsync======================================");
            // get entries that are being Added or Updated
            var modifiedEntries = ChangeTracker.Entries()
                    .Where(x => (x.State == EntityState.Added || x.State == EntityState.Modified));
            var userNameSystem = "";

            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows)){
                //userNameSystem= System.Security.Principal.WindowsIdentity.GetCurrent().Name;
                userNameSystem= string.Format("{0}- {1}-{2} DOMAIN: ({3})", Environment.UserName, Environment.OSVersion, Environment.MachineName, Environment.UserDomainName);
            }

            if (!RuntimeInformation.IsOSPlatform(OSPlatform.Windows)){
                userNameSystem= string.Format("{0}- {1}-{2} DOMAIN: ({3})", Environment.UserName, Environment.OSVersion, Environment.MachineName, Environment.UserDomainName);
            }


            
            // var userIdentity = _httpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier);
            //if(_httpContextAccessor.HttpContext)
            var userIdentity = "";
            if(_httpContextAccessor.HttpContext !=null && _httpContextAccessor.HttpContext.User!=null) {
                userIdentity = _httpContextAccessor.HttpContext.User.GetLoggedInUserId<string>();
            }
            var now = DateTime.Now;
            foreach (var entry in modifiedEntries)
            {
                // var entity = entry.Entity as AuditableEntity;
                var verificatePropertyAudityExist = Util.HasProperty(entry.Entity, "CreatedDate");
                if (verificatePropertyAudityExist)
                {
                    if (entry.State == EntityState.Added)
                    {
                        entry.Entity.GetType().GetProperty("CreatedBySystem").SetValue(entry.Entity, String.IsNullOrEmpty(userNameSystem)  ? "unknown":userNameSystem, null);
                        entry.Entity.GetType().GetProperty("CreatedBy").SetValue(entry.Entity, String.IsNullOrEmpty(userIdentity)  ? "unknown":userIdentity, null);
                        entry.Entity.GetType().GetProperty("CreatedDate").SetValue(entry.Entity, now, null);
                        /* entity.CreatedBySystem = userNameSystem ?? "unknown";
                        entity.CreatedByIdentity = userIdentity ?? "unknown";
                        entity.CreatedDate = now; */
                    }
                    else
                    {
                        /* entity.UpdateBySystem = userNameSystem ?? "unknown";
                        entity.UpdatedByIdentity = userIdentity ?? "unknown";
                        entity.UpdatedDate = now; */
                        entry.Entity.GetType().GetProperty("LastModifiedBySystem").SetValue(entry.Entity, String.IsNullOrEmpty(userNameSystem)  ? "unknown":userNameSystem, null);
                        entry.Entity.GetType().GetProperty("LastModifiedBy").SetValue(entry.Entity,String.IsNullOrEmpty(userIdentity)  ? "unknown":userIdentity, null);
                        entry.Entity.GetType().GetProperty("LastModifiedDate").SetValue(entry.Entity, now, null);
                    }
                }
            }
            return await base.SaveChangesAsync(cancellationToken);
            /// return await _helper.SaveChangesAsync(_auditContext, () => base.SaveChangesAsync(cancellationToken));
        }
        #endregion

        public DbSet<ItemList> ItemLists { get; set; }
        public DbSet<TypeList> TypeLists { get; set; }
        public DbSet<Patient> Patients { get; set; }
        public DbSet<MedicalConsultation> MedicalConsultations { get; set; }
    }
}