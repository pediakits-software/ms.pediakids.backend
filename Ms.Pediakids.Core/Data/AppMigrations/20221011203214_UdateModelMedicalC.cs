﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Ms.Pediakids.Core.Data.AppMigrations
{
    public partial class UdateModelMedicalC : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<bool>(
                name: "Active",
                schema: "public",
                table: "ItemList",
                nullable: true,
                defaultValueSql: "true",
                oldClrType: typeof(bool),
                oldType: "boolean",
                oldDefaultValueSql: "true");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<bool>(
                name: "Active",
                schema: "public",
                table: "ItemList",
                type: "boolean",
                nullable: false,
                defaultValueSql: "true",
                oldClrType: typeof(bool),
                oldNullable: true,
                oldDefaultValueSql: "true");
        }
    }
}
