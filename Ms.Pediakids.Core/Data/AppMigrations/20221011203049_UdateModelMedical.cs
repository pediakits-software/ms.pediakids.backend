﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Ms.Pediakids.Core.Data.AppMigrations
{
    public partial class UdateModelMedical : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "TencionArterial",
                schema: "public",
                table: "MedicalConsultation",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AlterColumn<int>(
                name: "Saturacion",
                schema: "public",
                table: "MedicalConsultation",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AlterColumn<string>(
                name: "Peso",
                schema: "public",
                table: "MedicalConsultation",
                maxLength: 20,
                nullable: false,
                oldClrType: typeof(double),
                oldType: "double precision");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "TencionArterial",
                schema: "public",
                table: "MedicalConsultation",
                type: "integer",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "Saturacion",
                schema: "public",
                table: "MedicalConsultation",
                type: "integer",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<double>(
                name: "Peso",
                schema: "public",
                table: "MedicalConsultation",
                type: "double precision",
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 20);
        }
    }
}
