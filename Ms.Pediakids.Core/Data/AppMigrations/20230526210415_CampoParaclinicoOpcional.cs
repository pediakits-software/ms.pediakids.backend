﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Ms.Pediakids.Core.Data.AppMigrations
{
    public partial class CampoParaclinicoOpcional : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ParaclinicosOpcional",
                schema: "public",
                table: "MedicalConsultation",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ParaclinicosOpcional",
                schema: "public",
                table: "MedicalConsultation");
        }
    }
}
