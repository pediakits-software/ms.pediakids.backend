﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace Ms.Pediakids.Core.Data.AppMigrations
{
    public partial class AppInitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "public");

            migrationBuilder.CreateTable(
                name: "TypeList",
                schema: "public",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedBySystem = table.Column<string>(nullable: true),
                    LastModifiedDate = table.Column<DateTime>(nullable: true),
                    LastModifiedBy = table.Column<string>(nullable: true),
                    LastModifiedBySystem = table.Column<string>(nullable: true),
                    Name = table.Column<string>(maxLength: 40, nullable: true),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TypeList", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ItemList",
                schema: "public",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedBySystem = table.Column<string>(nullable: true),
                    LastModifiedDate = table.Column<DateTime>(nullable: true),
                    LastModifiedBy = table.Column<string>(nullable: true),
                    LastModifiedBySystem = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Properties = table.Column<string>(type: "json", nullable: true),
                    Code = table.Column<string>(maxLength: 30, nullable: true),
                    Active = table.Column<bool>(nullable: false, defaultValueSql: "true"),
                    TypeListId = table.Column<int>(nullable: false),
                    Owner_Id = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ItemList", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ItemList_ItemList_Owner_Id",
                        column: x => x.Owner_Id,
                        principalSchema: "public",
                        principalTable: "ItemList",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ItemList_TypeList_TypeListId",
                        column: x => x.TypeListId,
                        principalSchema: "public",
                        principalTable: "TypeList",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Patient",
                schema: "public",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedBySystem = table.Column<string>(nullable: true),
                    LastModifiedDate = table.Column<DateTime>(nullable: true),
                    LastModifiedBy = table.Column<string>(nullable: true),
                    LastModifiedBySystem = table.Column<string>(nullable: true),
                    Nombres = table.Column<string>(maxLength: 50, nullable: false),
                    Apellidos = table.Column<string>(maxLength: 50, nullable: false),
                    NumeroIdentificacion = table.Column<string>(maxLength: 20, nullable: false),
                    FechaNacimiento = table.Column<DateTime>(nullable: false),
                    Direccion = table.Column<string>(maxLength: 100, nullable: true),
                    Telefono = table.Column<string>(maxLength: 20, nullable: true),
                    TipoIdentificacionId = table.Column<int>(nullable: false),
                    GeneroId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Patient", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Patient_ItemList_GeneroId",
                        column: x => x.GeneroId,
                        principalSchema: "public",
                        principalTable: "ItemList",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Patient_ItemList_TipoIdentificacionId",
                        column: x => x.TipoIdentificacionId,
                        principalSchema: "public",
                        principalTable: "ItemList",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MedicalConsultation",
                schema: "public",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedBySystem = table.Column<string>(nullable: true),
                    LastModifiedDate = table.Column<DateTime>(nullable: true),
                    LastModifiedBy = table.Column<string>(nullable: true),
                    LastModifiedBySystem = table.Column<string>(nullable: true),
                    NumeroIdentificacion = table.Column<string>(maxLength: 20, nullable: false),
                    Motivo = table.Column<string>(nullable: false),
                    EvolucionEnfermedad = table.Column<string>(nullable: false),
                    Antecedentes = table.Column<string>(nullable: false),
                    Peso = table.Column<double>(nullable: false),
                    Talla = table.Column<int>(nullable: false),
                    PerimetoCefalico = table.Column<double>(nullable: false),
                    FrecuenciaCardiaca = table.Column<int>(nullable: false),
                    FrecuenciaResiratorio = table.Column<int>(nullable: false),
                    Temperatura = table.Column<double>(nullable: false),
                    Saturacion = table.Column<int>(nullable: false),
                    TencionArterial = table.Column<int>(nullable: false),
                    Cabeza = table.Column<string>(maxLength: 500, nullable: true),
                    ORL = table.Column<string>(maxLength: 500, nullable: true),
                    Cuello = table.Column<string>(maxLength: 500, nullable: true),
                    Torax = table.Column<string>(maxLength: 500, nullable: true),
                    Abdomen = table.Column<string>(maxLength: 500, nullable: true),
                    Genitales = table.Column<string>(maxLength: 500, nullable: true),
                    Extremidades = table.Column<string>(maxLength: 500, nullable: true),
                    Piel = table.Column<string>(maxLength: 500, nullable: true),
                    Neurologico = table.Column<string>(maxLength: 500, nullable: true),
                    Diagnostico = table.Column<string>(nullable: false),
                    Tratamiento = table.Column<string>(nullable: true),
                    Paraclinicos = table.Column<string>(nullable: true),
                    PatientId = table.Column<int>(nullable: false),
                    TipoIdentificacionId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MedicalConsultation", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MedicalConsultation_Patient_PatientId",
                        column: x => x.PatientId,
                        principalSchema: "public",
                        principalTable: "Patient",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MedicalConsultation_ItemList_TipoIdentificacionId",
                        column: x => x.TipoIdentificacionId,
                        principalSchema: "public",
                        principalTable: "ItemList",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ItemList_Code",
                schema: "public",
                table: "ItemList",
                column: "Code",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ItemList_Owner_Id",
                schema: "public",
                table: "ItemList",
                column: "Owner_Id");

            migrationBuilder.CreateIndex(
                name: "IX_ItemList_TypeListId",
                schema: "public",
                table: "ItemList",
                column: "TypeListId");

            migrationBuilder.CreateIndex(
                name: "IX_MedicalConsultation_PatientId",
                schema: "public",
                table: "MedicalConsultation",
                column: "PatientId");

            migrationBuilder.CreateIndex(
                name: "IX_MedicalConsultation_TipoIdentificacionId",
                schema: "public",
                table: "MedicalConsultation",
                column: "TipoIdentificacionId");

            migrationBuilder.CreateIndex(
                name: "IX_Patient_GeneroId",
                schema: "public",
                table: "Patient",
                column: "GeneroId");

            migrationBuilder.CreateIndex(
                name: "IX_Patient_TipoIdentificacionId",
                schema: "public",
                table: "Patient",
                column: "TipoIdentificacionId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MedicalConsultation",
                schema: "public");

            migrationBuilder.DropTable(
                name: "Patient",
                schema: "public");

            migrationBuilder.DropTable(
                name: "ItemList",
                schema: "public");

            migrationBuilder.DropTable(
                name: "TypeList",
                schema: "public");
        }
    }
}
