﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Ms.Pediakids.Core.Data.AppMigrations
{
    public partial class CampoNombreComplerequired : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "NombreCompleto",
                schema: "public",
                table: "Patient",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "text");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "NombreCompleto",
                schema: "public",
                table: "Patient",
                type: "text",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
