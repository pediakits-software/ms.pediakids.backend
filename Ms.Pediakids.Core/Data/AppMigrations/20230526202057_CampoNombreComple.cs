﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Ms.Pediakids.Core.Data.AppMigrations
{
    public partial class CampoNombreComple : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "NombreCompleto",
                schema: "public",
                table: "Patient",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "NombreCompleto",
                schema: "public",
                table: "Patient");
        }
    }
}
