using System;
using System.ComponentModel.DataAnnotations;

namespace Ms.Pediakids.Core.DTO
{
    public class PatientDTO
    { 
        [Required(ErrorMessage = "El campo {0} es obligatorio")] 
        [StringLength(50, ErrorMessage = "El campo {0}, no puede superar los {1} caracteres")]
        public string Nombres { get; set; }    
        [StringLength(50, ErrorMessage = "El campo {0}, no puede superar los {1} caracteres")]
        [Required(ErrorMessage = "El campo {0} es obligatorio")] 
        public string Apellidos { get; set; }   
        [Required(ErrorMessage = "El campo {0} es obligatorio")] 
        public int TipoIdentificacion { get; set; }
        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [StringLength(20, ErrorMessage = "El campo {0}, no puede superar los {1} caracteres")]
        public string NumeroIdentificacion { get; set; }
        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        public DateTime FechaNacimiento { get; set; }
        [StringLength(100, ErrorMessage = "El campo {0}, no puede superar los {1} caracteres")]
        public string Direccion { get; set; }
        [StringLength(20, ErrorMessage = "El campo {0}, no puede superar los {1} caracteres")]
        public string Telefono { get; set; }
    }
}